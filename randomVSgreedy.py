# -*- coding: utf-8 -*-
from Reversi import Board
from random import randint, choice

def randomMove(b):
    '''Renvoie un mouvement au hasard sur la liste des mouvements possibles. Pour avoir un choix au hasard, il faut
    construire explicitement tous les mouvements. Or, generate_legal_moves() nous donne un itÃ©rateur.'''
    return choice([m for m in b.legal_moves()])

def randomMoves(b):
    '''Renvoie un mouvement au hasard sur la liste des mouvements possibles. Pour avoir un choix au hasard, il faut
    construire explicitement tous les mouvements. Or, generate_legal_moves() nous donne un itÃ©rateur.'''
    return [m for m in b.legal_moves()]


def coin_parity(b):
    white_pieces,black_pieces = b.get_nb_pieces()

    evaluation = white_pieces - black_pieces

    return evaluation


def evaluer_position(b):
    #coin_point = coin_parity(b)
    return Board.heuristique(b)
    #return coin_point


def maxMin(b,profondeur,alpha,beta):
    if profondeur==0 or b.is_game_over():
        res = evaluer_position(b)
        return res
    best = float("-inf")

    for coup in randomMoves(b):
        b.push(coup)
        best = max(best,minMax(b,profondeur-1,alpha,beta))
        b.pop()
        alpha = max(alpha,best)
        if alpha>=best:
            break
    return best

def minMax(b,profondeur,alpha,beta):
    if profondeur==0 or b.is_game_over():
        res = evaluer_position(b)
        return res
    worst = float("inf")

    for coup in randomMoves(b):
        b.push(coup)
        worst = min(worst,maxMin(b,profondeur-1, alpha, beta))
        b.pop()
        beta = min(beta, worst)
        if alpha >= beta:
            break
    return worst

def greedyMove(b,profondeur):
    meilleur_coup = None
    turn = b._nextPlayer
    meilleur_coup_eval = float("-inf") if turn == b._WHITE else float("inf")
    alpha = -float("inf")
    beta = float("inf")

    for coup in randomMoves(b):
        b.push(coup)
        eval_coup = minMax(b, profondeur - 1, alpha, beta) if turn == b._BLACK else maxMin(b, profondeur - 1, alpha, beta)
        b.pop()

        if (turn == b._WHITE and eval_coup > meilleur_coup_eval) or (turn == b._BLACK and eval_coup < meilleur_coup_eval):
            meilleur_coup = coup
            meilleur_coup_eval = eval_coup

    return meilleur_coup
    

def deroulementRandom(b,result):
    #print("----------")
    #print(b)
    if b.is_game_over():
        white_pieces,black_pieces = b.get_nb_pieces()
        if white_pieces>black_pieces:
            result[0]+=1
        if white_pieces<black_pieces:
            result[1]+=1
        if white_pieces==black_pieces:
            result[2]+=1
        return
    if b._nextPlayer == b._WHITE:
        b.push(randomMove(b))
    else:
        b.push(greedyMove(b,2))
    deroulementRandom(b,result)
    b.pop()


#board = Board()
#result = [0,0,0]
#for i in range(100):
#    deroulementRandom(board,result)
#print("white:",result[0],"black:",result[1],"draw:",result[2],)