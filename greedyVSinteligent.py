# -*- coding: utf-8 -*-
import sys
from Reversi import Board
from random import randint, choice
from randomVSgreedy import greedyMove
import time
import matplotlib.pyplot as plt

def randomMove(b):
    '''Renvoie un mouvement au hasard sur la liste des mouvements possibles. Pour avoir un choix au hasard, il faut
    construire explicitement tous les mouvements. Or, generate_legal_moves() nous donne un itÃ©rateur.'''
    return choice([m for m in b.legal_moves()])

def randomMoves(b):
    '''Renvoie un mouvement au hasard sur la liste des mouvements possibles. Pour avoir un choix au hasard, il faut
    construire explicitement tous les mouvements. Or, generate_legal_moves() nous donne un itÃ©rateur.'''
    return [m for m in b.legal_moves()]


def evaluate_coin_parity(b):
    white_pieces,black_pieces = b.get_nb_pieces()
    if (white_pieces + black_pieces) !=0:
        evaluation = 100*(white_pieces - black_pieces)/(white_pieces + black_pieces)
    else:
        evaluation = 0
    return evaluation

def count_empty_neighbors(b, row, col):
    empty_neighbors = 0
    directions = [(1, 0), (-1, 0), (0, 1), (0, -1), (1, 1), (-1, -1), (1, -1), (-1, 1)]

    for dr, dc in directions:
        r, c = row + dr, col + dc
        if 0 <= r < b.get_board_size() and 0 <= c < b.get_board_size() and b._board[r][c] == b._EMPTY:
            empty_neighbors += 1

    return empty_neighbors

def evaluate_black_potential_mobility(b):
    potential_mobility = 0
    for row in range(b.get_board_size()):
        for col in range(b.get_board_size()):
            if b._board[row][col] == b._BLACK:
                potential_mobility += count_empty_neighbors(b, row, col)

    return potential_mobility

def evaluate_white_potential_mobility(b):
    potential_mobility = 0
    for row in range(b.get_board_size()):
        for col in range(b.get_board_size()):
            if b._board[row][col] == b._WHITE:
                potential_mobility += count_empty_neighbors(b, row, col)

    return potential_mobility

def evaluate_mobility(b):
    if b._nextPlayer == b._WHITE:
        white_actual_mobility = len(b.legal_moves())
        b._nextPlayer = b._flip(b._nextPlayer)
        black_actual_mobility = len(b.legal_moves())
        b._nextPlayer = b._flip(b._nextPlayer)
    else :
        black_actual_mobility = len(b.legal_moves())
        b._nextPlayer = b._flip(b._nextPlayer)
        white_actual_mobility = len(b.legal_moves())
        b._nextPlayer = b._flip(b._nextPlayer)

    white_potential_mobility = evaluate_white_potential_mobility(b)
    black_potential_mobility = evaluate_black_potential_mobility(b)

    if (white_actual_mobility+black_actual_mobility) !=0:
        actual_mobility_value = 100*(white_actual_mobility-black_actual_mobility)/(white_actual_mobility+black_actual_mobility)
    else:
        actual_mobility_value = 0

    if (white_potential_mobility+black_potential_mobility) !=0:
        potential_mobility_value = 100*(white_potential_mobility-black_potential_mobility)/(white_potential_mobility+black_potential_mobility)
    else:
        potential_mobility_value = 0

    evaluation = actual_mobility_value + potential_mobility_value
    return evaluation

def is_corner(b,row,col):
    return (row==0 and col==0) or (row==0 and col==b.get_board_size()-1) or (row==b.get_board_size()-1 and col==0) or (row==b.get_board_size()-1 and col==b.get_board_size()-1)

def evaluate_corners(b):
    white_player_corner_value = 0
    black_player_corner_value = 0

    for row in range(b.get_board_size()):
        for col in range(b.get_board_size()):
            if is_corner(b,row, col):
                if b._board[row][col] == b._WHITE:
                    white_player_corner_value += 1
                elif b._board[row][col] == b._BLACK:
                    black_player_corner_value += 1

    if (white_player_corner_value+black_player_corner_value)!=0:
        evaluation = 100*(white_player_corner_value-black_player_corner_value)/(white_player_corner_value+black_player_corner_value)
    else:
        evaluation = 0
    return evaluation

def evaluer_position(b):
    coin_point = evaluate_coin_parity(b)
    mobility = evaluate_mobility(b)
    corners = evaluate_corners(b)
    return coin_point + mobility + corners


def maxMin(b,profondeur,alpha,beta):
    if profondeur==0 or b.is_game_over():
        res = evaluer_position(b)
        return res
    best = float("-inf")

    for coup in randomMoves(b):
        b.push(coup)
        best = max(best,minMax(b,profondeur-1,alpha,beta))
        b.pop()
        alpha = max(alpha,best)
        if alpha>=best:
            break
    return best

def minMax(b,profondeur,alpha,beta):
    if profondeur==0 or b.is_game_over():
        res = evaluer_position(b)
        return res
    worst = float("inf")

    for coup in randomMoves(b):
        b.push(coup)
        worst = min(worst,maxMin(b,profondeur-1, alpha, beta))
        b.pop()
        beta = min(beta, worst)
        if alpha >= beta:
            break
    return worst

def bestMove(b,profondeur):
    meilleur_coup = None
    turn = b._nextPlayer
    meilleur_coup_eval = float("-inf") if turn == b._WHITE else float("inf")
    alpha = -float("inf")
    beta = float("inf")

    for coup in randomMoves(b):
        b.push(coup)
        eval_coup = minMax(b, profondeur - 1, alpha, beta) if turn == b._BLACK else maxMin(b, profondeur - 1, alpha, beta)
        b.pop()

        if (turn == b._WHITE and eval_coup > meilleur_coup_eval) or (turn == b._BLACK and eval_coup < meilleur_coup_eval):
            meilleur_coup = coup
            meilleur_coup_eval = eval_coup

    return meilleur_coup

def humanMove(b):
    coups = b.legal_moves()
    for i in range(len(coups)):
        print(f"{i}:{coups[i]}")

    
    user_input = -1

    while user_input < 0 or user_input >= len(coups):
        try:
            user_input = int(input(f"Enter a number between 0 and {len(coups) - 1} : "))
        except ValueError:
            print("Invalid input. Please enter a valid integer.")


    return coups[user_input]

    

def deroulementRandom(b,result):
    #print("----------")
    #print(b)
    if b.is_game_over():
        white_pieces,black_pieces = b.get_nb_pieces()
        if white_pieces>black_pieces:
            print("white win")
            result[0]+=1
        if white_pieces<black_pieces:
            print("black win")
            result[1]+=1
        if white_pieces==black_pieces:
            print("draw")
            result[2]+=1
        return
    if b._nextPlayer == b._WHITE:
        b.push(randomMove(b))
    else:
        b.push(bestMove(b,3))
    deroulementRandom(b,result)
    b.pop()


board = Board()
result = [0,0,0]

for i in range(10):
    timestart = time.perf_counter()
    deroulementRandom(board,result)
    timeend = time.perf_counter()
    timegame = timeend-timestart
    print(f"game {i} timer:",timegame,"s")
    print("------------------------")
    
total = sum(result)
winrate = result[1] / total * 100
print("white:", result[0], "black:", result[1], "draw:", result[2], "winrate:", winrate, "%")

# Create a pie chart
labels = ['White Wins', 'Black Wins', 'Draws']
colors = ['lightcoral', 'lightblue', 'lightgreen']
explode = (0.1, 0, 0)  # explode the 1st slice (i.e., 'White Wins')

plt.pie(result, explode=explode, labels=labels, colors=colors, autopct='%1.1f%%', shadow=True, startangle=140)
plt.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
plt.title('Game Results and Winrate')
plt.show()